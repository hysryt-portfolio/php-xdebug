FROM php:7.2-apache

RUN pecl install xdebug-2.6.1 && docker-php-ext-enable xdebug
ADD ./xdebug.ini $PHP_INI_DIR/conf.d/